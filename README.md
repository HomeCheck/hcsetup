
# How to setup a debian for Homecheck Server

The following distributions are supported and tested:
* Debian linux version >9
* Ubuntu linux 20, 22
Hardware:
* AMD64
* ARM64, Raspberry PI, version >3B

When You want to install MongoDB on RPi, currently you need to use ubuntu server 20.04.5 64bit version! Other distributions 
(Debian, CentOS, Raspbian) does not have APT version of mongo-org package. If you have time, you can try to compile from source. Have fun   

For Raspberry you need to install 64-bit version of Ubuntu, choose the best fit for your Raspberry. 
Only version 3+ are able to run 64-bit linux. 


## Before install
After setting up your linux you need to login with your default user, and create a directory for HCSetup: 
    
    chdir HCSetup 

Copy files to HCSetup directory using scp or similar software (WinScp)

## Install
Run any of the file starting with setup_ based on your need

Environment can be: dev, prod
Distribution: ubuntu, debian

    sudo bash ./setup.sh <environment> <distribution>

This will setup environment for HomeCheck server

##Jenkins post-install
Edit Jenkins config file and increase start timeout time, otherwise it won't start

    sudo systemctl edit --full jenkins.service

Restart jenkins

    sudo systemctl restart jenkins

## Deployment

Either you use
You need to deploy HomeCheck Server to /www/prod. File deploy_prod.sh is useful for deploying. Check its manual.

## Post Deployment

Once deployment is done to /www/prod, you need to run firstrun.sh to finish the whole process.
It makes a final setup and run 1 instance of the server and the server monitor.

Have fun!