#!/bin/bash

source ./bootstrap/utils.sh

# *************** Setup sftp server
h1 "Setting up SFTP Server"
# make backup
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.old

# comment out unnecessary lines from ssh_config
# the lines below were removed because sftp user need torun commands too, and
# altering ssh config makes it impossible, uncomment, when found a new solution

#if grep  "#Subsystem\s*sftp\s*/usr/lib/openssh/sftp-server"  /etc/ssh/sshd_config; then
#  echo -e "* sshd_config is already altered."
#else
#  sudo sed -i -E 's/(Subsystem\s*sftp\s*\/usr\/lib\/openssh\/sftp-server)/#\1/g' /etc/ssh/sshd_config#
#
#  # extend sftp config
#  sudo cat <<EOF >> /etc/ssh/sshd_config
#  Subsystem       sftp    internal-sftp
#
#  Match Group sftp_users
#    X11Forwarding no
#    AllowTcpForwarding no
#    ChrootDirectory %h
#    ForceCommand internal-sftp
#EOF
#fi

# restart ssh daemon
h2 "Restarting SSH daemon"
sudo systemctl restart sshd
