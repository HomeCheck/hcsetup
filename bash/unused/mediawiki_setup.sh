#!/bin/bash

source ./bootstrap/utils.sh

mediawiki_dbname="mediawiki"
mediawiki_username="mediawiki"
mediawiki_password="sentinel123"
mediawiki_root_password="Homecheck.647.Geek"
mounted_drive_location="/mnt/volume_fra1_01"
mysql_config_file="/etc/mysql/mariadb.conf.d/50-server.cnf"
mysql_database_path="/var/lib/mysql"
mysql_new_database_path="$mounted_drive_location/mysql"

env=$1

mediawiki_folder_path="./mediawiki"

skin_aether_git_url="https://invent.kde.org/websites/aether-mediawiki.git"
skin_original_dir="/var/lib/mediawiki/skins"
skin_aether_dir_name="Aether"
skin_aether_original_path="$skin_original_dir/$skin_aether_dir_name"
skin_aether_path="$mediawiki_folder_path/$skin_aether_dir_name"

h1 "Installing mediawiki"
sudo apt-get install -y -qq mediawiki

# If prod environment, copy mysql db to shared drive, otherwise leave it where it is
if [ "$env" == "prod" ]; then
    h2 "Environment $h2$env$no: copying database to mounted drive"
    if [ ! -d "$mounted_drive_location" ]; then
    h2 "Directory $h2$mounted_drive_location$no does not exist, creating"
        sudo mkdir -p "$mounted_drive_location"
        sudo chown -R mysql "$mounted_drive_location"
        sudo chgrp -R mysql "$mounted_drive_location"
    fi

    h2 "Shutting down mysql daemon"
    sudo systemctl stop mysql

    # copy mysql db to the new location
    h2 "Moving existing database directory ($h2$mysql_database_path$no) to $h2$mounted_drive_location$no"
    sudo rsync -av -q "$mysql_database_path" "$mounted_drive_location"

    # alter mysql.conf
    if  grep -q "datadir\s*=\s*${mounted_drive_location}" "$mysql_config_file" ; then
        h2 "Datadir already set, no change required"
    else
        h2 "Updating mysql config"
        sudo sed -i -e "s|\(\s*datadir\s*=\s*\)${mysql_database_path}|\1${mysql_new_database_path}|g" "$mysql_config_file"
    fi

    h2 "Starting up mysql daemon"
    sudo systemctl start mysql
fi

h2 "Creating mediawiki database $h2$mediawiki_dbname$no"
# perhaps it would be better if we remove the whole database before creating new one
SQL "CREATE DATABASE IF NOT EXISTS $mediawiki_dbname CHARACTER SET utf8 COLLATE utf8_hungarian_ci;"

h2 "Creating user $h2$mediawiki_username$no"
SQL "CREATE USER IF NOT EXISTS $mediawiki_username@localhost IDENTIFIED BY '$mediawiki_password';"

h2 "Setting up privilieges of user $h2$mediawiki_username$no for database $h2$mediawiki_dbname$no"
SQL "GRANT ALL PRIVILEGES ON $mediawiki_dbname.* TO '$mediawiki_username'@'localhost';"
SQL "FLUSH PRIVILEGES;"

h2 "Dealing with skin Aether $skin_aether_original_path"
if [ ! -d "$skin_aether_original_path" ]; then
    if [ ! -d "$skin_aether_path"  ]; then
        h3 "Cloning repo $b$skin_aether_git_url$no to directory $b$mediawiki_folder_path/$skin_aether_dir_name$no"
        git clone "$skin_aether_git_url" "$mediawiki_folder_path/$skin_aether_dir_name" --quiet
    fi
        
    h3 "Moving skin directory to mediawiki skin folder"
    sudo mv "$skin_aether_path" "$skin_original_dir"
else 
    h3 "Skin already downloaded, aborting process"
fi

h2 "IMPORTANT: run LocalSettings Generation after Mediawiki was installed"

h2 "Installing Composer temporarily"
sudo apt-get install composer -y -qq
h3 "Composing php"
cd "$skin_aether_original_path"
composer install

#should we set root password or leave a security hole?
#mysqladmin -uroot password "${mediawiki_root_password}"
