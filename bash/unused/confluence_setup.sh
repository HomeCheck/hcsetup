#!/bin/bash

source ./bootstrap/utils.sh

mounted_drive_location="/mnt/volume_fra1_01"
mysql_config_file="/etc/mysql/mariadb.conf.d/50-server.cnf"
mysql_database_path="/var/lib/mysql"
mysql_new_database_path="$mounted_drive_location/mysql"

# environment variable
env=$1

h1 "Installing Mysql server"
sudo apt-get install mysql-server

# If prod environment, copy mysql db to shared drive, otherwise leave it where it is
if [ "$env" == "prod" ]; then
    h2 "Environment $h2$env$no: copying database to mounted drive"
    if [ ! -d "$mounted_drive_location" ]; then
    h2 "Directory $h2$mounted_drive_location$no does not exist, creating"
        sudo mkdir -p "$mounted_drive_location"
        sudo chown -R mysql "$mounted_drive_location"
        sudo chgrp -R mysql "$mounted_drive_location"
    fi

    h2 "Shutting down mysql daemon"
    sudo systemctl stop mysql

    # copy mysql db to the new location
    h2 "Moving existing database directory ($h2$mysql_database_path$no) to $h2$mounted_drive_location$no"
    sudo rsync -av -q "$mysql_database_path" "$mounted_drive_location"

    # alter mysql.conf
    if  grep -q "datadir\s*=\s*${mounted_drive_location}" "$mysql_config_file" ; then
        h2 "Datadir already set, no change required"
    else
        h2 "Updating mysql config"
        sudo sed -i -e "s|\(\s*datadir\s*=\s*\)${mysql_database_path}|\1${mysql_new_database_path}|g" "$mysql_config_file"
    fi

    h2 "Starting up mysql daemon"
    sudo systemctl start mysql
fi

h1 "Installing java"
sudo apt-get install default-jdk -y -qq
