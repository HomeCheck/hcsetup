#!/bin/bash

# env can be:
#   - dev: development env on any device
#   - vagrant: specific to vagrant
#   - prod: production


source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

defEnv=dev
env=${1:-$defEnv}

linuxDist=${2:-$linux_dist}

// isRaspi is
if grep  "Model\s*:\s*Raspberry"  /proc/cpuinfo; then
    isRaspi=true
else
    isRaspi=false
fi

h1 "Setting up Jenkins Deploy system and HomeCheck Server"
h2 "Environment: ${env}"
h2 "Is it Raspberry host?: ${isRaspi}"
h2 "Linux distribution: ${linuxDist}"
h2 "Linux dist codename: ${codename}"
h1 "Updating apt ..."
apt-get update -y -qq
apt-get upgrade -y -qq

# when installing using vagrant, the directory "vagrant" is the shared folder, where all the files are
# otherwise bootstrap file can be anywhere.
if [[ $env = "vagrant" ]]; then
    cd /vagrant
fi

export DEBIAN_FRONTEND=noninteractive
# creates user tkovari,deployer (ssh auth key, password, home),
# sftp group, add deployer to sftp users
sudo bash ./bootstrap/user_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up users failed";
    exit 1
fi

# this comes before deploy and mongo and others
# installs git, mc, docker
sudo bash ./bootstrap/tools_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up tools failed";
    exit 1
fi

# installs nvm and npm
sudo bash ./bootstrap/npm_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up nvm/npm failed";
    exit 1
fi

# installs npm libraries
sudo bash ./bootstrap/npm_libraries_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up npm libraries failed";
    exit 1
fi

# creates dir upload, prod, log, monitor in /www,
# initializes git repo in /www/prod, set owner for .git dir
sudo bash ./bootstrap/deploy_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up deploy related stuff failed";
    exit 1
fi

# install mosquito and setup
sudo bash ./bootstrap/mosquitto_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up Mosquitto failed";
    exit 1
fi

# install mongodb, set db path if necessary
#sudo bash ./bootstrap/mongo_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up MongoDB failed";
    exit 1
fi

# install Jenkins
sudo bash ./bootstrap/jenkins_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up Jenkins failed";
    exit 1
fi

# install apache
sudo bash ./bootstrap/apache_setup.sh "${env}" "${isRaspi}" "${linuxDist}" "${codename}"

if [[ $? != 0 ]]; then
    echo "Setting up Apache failed";
    exit 1
fi


echo -e "${h1}Done.$no\n"
