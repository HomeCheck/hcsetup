#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

env=$1
isRaspi=$2
linuxDist=$3
codename=$4


jenkins_username="jenkins"
jdk_version=17
jenkins_config_path="/etc/systemd/system/jenkins.service"
timeoutStartSec=678

h1 "Installing Jenkins..."
h2 "Installing OpenJDK-$jdk_version"
sudo apt install openjdk-"$jdk_version"-jre -qy # openjdk version might be different in the future! check before

h2 "Adding Jenkins repository key..."
# debian is ok for ubuntu too (not verified for other dists)
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

h2 "Adding Jenkins Repository to the list..."
sudo sh -c "echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list"

h2 "Updating apt again..."
sudo apt update -qy -qq

h2 "Installing Jenkins..."
sudo apt install jenkins -qy -qq

h2 "Enabling Jenkins with systemctl"
sudo systemctl unmask jenkins
sudo systemctl enable jenkins


h2 "Putting user jenkins to docker group"
# make jenkins the member of docker
sudo usermod -a -G docker "${jenkins_username}"
# make jenkins the member of deployer
sudo usermod -a -G "${deployer_username}" "${jenkins_username}"
# make jenkins a sudoer, so job scripts can use sudo command
sudo usermod -aG sudo jenkins
# remove jenkins passwd, so sudo in job scripts wont ask for password
sudo passwd -d jenkins


h2 "Setting up Jenkins config"
# if timeout is commented out then remove comment, set time
if grep -q "^\s*#TimeoutStartSec" "$jenkins_config_path"; then
    h3 "Timeout is commented out, setting up timeout"
    sudo sed -i -E "s/(^\s*#TimeoutStartSec=.*)/TimeoutStartSec\=${timeoutStartSec}/g" "$jenkins_config_path"
else
    h3 "Timeout was set already, no action done"
fi

sudo systemctl daemon-reload
sudo systemctl start jenkins  # this may take 2 mins
# sudo systemctl edit jenkins # uncomment this if you want to change jenkins config during setup (some RPi needed more launch time then the default)

h2 "Add deployer user to jenkins group"
sudo usermod -a -G "$jenkins_username" "$deployer_username"


echo "For first run, use password $(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)"

