#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

env=$1
isRaspi=$2

# What this script does:
#   - creates upload dir for all uploads
#   - creates prod dir for live sites and servers
#   - creates deploy log dir
#
#creates
h1 "Installing deploy related libraries and setting up deploy environment"

# create upload directory, and set owner
create_dir "$upload_dir" "$deployer_username"
chmod 775 "$upload_dir"

# create prod directory for working production main library, and set owner
create_dir "$prod_dir" "$deployer_username"
chmod 775 "$prod_dir"

# create deployer log directory
create_dir "$deploy_log_dir" "$deployer_username"
chmod 775 "$deploy_log_dir"

#create base server deploy dir
create_dir "$server_deploy_dir" "$deployer_username"
chmod 775 "$server_deploy_dir"

# create server log directory
create_dir "$server_log_dir" "$deployer_username"
chmod 775 "$server_log_dir"

# create server upload dir
create_dir "$server_upload_dir" "$deployer_username"
chmod 775 "$server_upload_dir"

#create script dir and copy deploy script
create_dir "$server_script_dir" "$deployer_username"
chmod 775 "$server_script_dir"
create_dir "$server_script_dir"/bootstrap "$deployer_user"
chmod 775 "$server_script_dir"/bootstrap

# copy deploy scripts for HC Server and sites
h1 "copying deploy scripts"

# config/const/util files
cp bootstrap/utils.sh "$server_script_dir"/bootstrap
cp bootstrap/variables.sh "$server_script_dir"/bootstrap
# deployer files
cp deploy_server.sh "$server_script_dir"
cp deploy_site.sh "$server_script_dir"
cp deploy_helyzet_eu_provider.sh "$server_script_dir"

cp -R server_deploy "$server_script_dir"/server_deploy

# run pm2 
h1 "Setting up PM2"
# the 2 lines below should run without sudo!!!

echo "Running pm2 for user ${deployer_username}...";

# this will change to deployer and setup pm2, also config pm2 to start on boot with the "deployer" user and dir
su - "${deployer_username}" <<! >/dev/null 2>&1
pm2 startup > /dev/tty
!

# if this fails, it might be because of missing or wrong location of /usr/lib/node_modules/pm2/bin/pm2 , check whether it's /usr/local/lib/node_modules/pm2/bin/pm2
echo "${deployer_password}" | sudo -S env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u ${deployer_username} --hp ${deployer_dir} > /dev/tty
