#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

env=$1
isRaspi=$2

# homepages
declare -a homepageNames=("homecheck.hu" "helyzet.eu")

# directory where all the webpages are located
webRootBaseDir="/var/www"

# apache username
webUsername="www-data"

# Apache config root directory
apacheConfigRoot="/etc/apache2"

# Upload directory
upload_dir="/www/upload"

h1 "Installing and setting up Apache 2 web server"
# install apache server
sudo apt -y -qq install apache2 >install.apache.log &>install.apache.error.log


h1 "Removing default site settings"
find "${apacheConfigRoot}/sites-enabled" -type l -delete

h1 "Installing snapd"
sudo apt install snapd -y -qq

h2 "Installing core snap"
sudo snap install core
sudo snap refresh core

h2 "Installing certbot"
sudo snap install --classic certbot

# this creates a symlink to snap into /usr/bin/certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot

h2 "Creating common upload directory"
create_dir "${upload_dir}"

#
for i in "${homepageNames[@]}"
do
    echo "Creating settings $i"
    homepageName="$i"
    configFilename="${homepageName}.conf"
    websiteRoot="${webRootBaseDir}/${homepageName}"

    # Site config path
    siteConfigPath="$apacheConfigRoot/sites-available/$configFilename"
    siteConfigLinkPath="$apacheConfigRoot/sites-enabled/$configFilename"

    # create homepage root dirs
    create_dir "${websiteRoot}" "root"
    chgrp -R "$deployer_username" "${websiteRoot}"
    chmod 775 "${websiteRoot}"

    #create upload dirs
    create_dir "$upload_dir/$homepageName" "${deployer_username}"

    h2 "Copying config files for ${homepageName}"
    cp "apache/${configFilename}" "$siteConfigPath"

    h2 "Creating symlink for host ${homepageName}"
    ln -s "$siteConfigPath" "$siteConfigLinkPath"

    h2 "Getting certificate for ${homepageName}"
    sudo certbot --apache -d ${homepageName}
done


h1 "Enabling port 80 and 443"
iptables -I INPUT -p TCP --dport 80 -j LOG
iptables -I INPUT -p TCP --dport 80 -j ACCEPT
iptables -I INPUT -p TCP --dport 443 -j LOG
iptables -I INPUT -p TCP --dport 443 -j ACCEPT

h1 "Restarting apache server"
apachectl restart