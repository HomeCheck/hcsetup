#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

env=$1
isRaspi=$2
linuxDist=$3

mongoVersion="5.0"
mongodb_username="adminuser2"
mongodb_password="Sentinel.123"

ip=$(get_ip_addr)

# This setup file is obsolete as mongodb cannot be installed to Raspbian
# MongoDb can be installed to Ubuntu 20.x.x but it is very slow

# What this script does:
# - if raspberry/ubuntu:
#   - installs some tools for getting pgp key and use it
#   - get gpg key
#   - add mongo repository to apt list
#   - update and upgrade apt
#   - install mongodb
#   - reload systemctl daemon settings
#   - enables mongodb to launch on startup
# - if not raspberry (debian, ubuntu)
#   - adds repo to apt
#   - update, upgrade apt
#   - install mongodb
# - if prod
#   - mounts external storage and sets mongodb db location to that
# - setup mongodb config file
#   - volume location
# - if dev then

# install mongodb
h1 "Installing MongoDB server for environment'$env' raspi: $isRaspi"

# If Raspberry, it needs a different install method
if [[ "$isRaspi" = true ]]; then
    h2 "Raspberry detected"

    # install tools for installing mongodb, getting gpg key,
    sudo apt install dirmngr ca-certificates software-properties-common gnupg gnupg2 apt-transport-https curl -qy -qq
    if [[ $? != 0 ]]; then
        echo "Unable to install mongodb, exiting"
        exit 1
    fi

    # get install server from mongodb site
    wget -qO - https://www.mongodb.org/static/pgp/server-${mongoVersion}.asc | sudo apt-key add -
    if [[ $? != 0 ]]; then
        echo "Unable to install mongodb, failed to wget gpg key"
        exit 1
    fi

    #
    echo "" | sudo tee /etc/apt/sources.list.d/mongodb-org-${mongoVersion}.list
    echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/${linuxDist} ${codename}/mongodb-org/${mongoVersion} multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-"${mongoVersion}".list
    if [[ $? != 0 ]]; then
        echo "Unable to install mongodb, failed to create list file"
        exit 1
    fi

    sudo apt-get update -qy -qq
    if [[ $? != 0 ]]; then
        echo "Unable to install mongodb, apt update failed"
        exit 1
    fi

    sudo apt-get install -y mongodb-org
    if [[ $? != 0 ]]; then
        echo "Unable to install mongodb, apt install failed"
        exit 1
    fi

    sudo systemctl daemon-reload # refresh systemctl daemon to find mongodb
    sudo systemctl enable mongod # this will enable to load mongodb on startup

    # see comment above about isRaspi flag
    serviceName="mongod"
else
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 4B7C549A058F8B6B


    echo "deb http://repo.mongodb.org/apt/${linux_dist} ${codename}/mongodb-org/${mongoVersion} main" | sudo tee /etc/apt/sources.list.d/mongodb.list
    h2 "Update apt after adding mongodb package location"
    sudo apt-get update -y -qq
    sudo apt-get upgrade -qq -y

    h2 "Installing package"
    sudo apt-get -y -qq install mongodb-org
    # see comment above about isRaspi flag
    serviceName="mongod"
fi

# prod has a dedicated volume for databases, this part sets mongodb db path
h2 "Checking database path"
if [ "$env" = "prod" ]; then
    h3 "Mounting PROD volume '${prodVolumeName}'"
    mount -o discard,defaults,noatime /dev/disk/by-id/scsi-0DO_Volume_${prodVolumeName} /mnt/${prodVolumeName}
    echo "/dev/disk/by-id/scsi-0DO_Volume_volume-fra1-01 ${prodVolumeDir} ext4 defaults,nofail,discard 0 0" | sudo tee -a /etc/fstab
    mongo_db_path="$prodVolume"
else
    mongo_db_path="$otherVolume"
fi


# if mongodb db path is not set in config, create an entry for it (dbPath), dbPath is decided in the previous section
mongodb_config_path=$(get_mongo_config_path)
echo "MONGODB config path: $mongodb_config_path"
if grep -q "dbPath:\s*$mongo_db_path" "$mongodb_config_path"; then
    h3 "Mongo db already set."
else
    h3 "Config entry was not properly set, setting volume location to $mongo_db_path"
    sudo sed -i -E "s|(\s*dbPath\s*:\s*)(.*)|\1$mongo_db_path|g" "$mongodb_config_path"
    echo -e "sudo sed -i -E \"s|(\s*dbPath\s*:\s*)(.*)|\1$mongo_db_path|g\" \"$mongodb_config_path\""
fi

# instead of checking $ip, we should check bind_ip existence
# if dev environment, we allow remote connections, this will add it to config
if [[ "$env" = "dev" ]]; then
    h2 "Checking remote connection"

    if grep -q "^\s*bindIp:\s*127.0.0.1,\s*$ip" "$mongodb_config_path"; then
        h3 "Ip address already added to allowed hosts"
    else
        h3 "Adding IP to allowed hosts"
        sudo sed -i -E "s/(^\s*bindIp:.*)/\1,$ip/g" "$mongodb_config_path"
    fi
fi

# adding authentication to mongodb
h2 "Checking mongodb auth settings"
if grep -q "^\s*security:" "$mongodb_config_path"; then
    h3 "Security was found"
    
    if ! grep -q "^\s*authorization:\s*enabled" "$mongodb_config_path"; then
        h3 "But auth was not enabled, enabling"
        sudo sed -i -E 's/^(\s*security:)/\1\n  authorization: enabled/g' "$mongodb_config_path"
    else 
	h3 "All set, no action done."
    fi
else
    h3 "No security was set, adding security and auth line"
    echo -e "\nsecurity:\n  authorization: enabled" >> "$mongodb_config_path";
fi

#h2 "Wait for 3secs, until mongodb gets started"
sudo systemctl start "${serviceName}.service"
sleep 7

h2 "Adding admin user ($mongodb_username) - this will fail if you run setup twice";
mongo --quiet --eval "db.createUser({ user: \"$mongodb_username\", pwd: \"$mongodb_password\", roles: [ { role: \"userAdminAnyDatabase\", db: \"admin\" }, \"readWriteAnyDatabase\" ] } )" admin
h3 "done"

h2 "Stopping MongoDB service"
sudo systemctl stop "${serviceName}.service"
h2 "Starting MongoDB service"
sudo systemctl start "${serviceName}.service"
h2 "Setting up MongoDB service"
sudo systemctl enable "${serviceName}.service"
