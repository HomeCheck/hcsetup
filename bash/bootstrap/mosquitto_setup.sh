#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

linuxDist=$3
codeName=$4

mosquitto_list_filename="mosquitto-${codename}.list"
mosquitto_key_filename="mosquitto-repo.gpg.key"

# check whether it is possible to remove all, as apt has a package for it
h1 "Installing mosquitto release"
# get mosquitto repository package signing key

if [[ "${linuxDist}" == "ubuntu" ]]; then
    sudo wget "https://launchpad.net/~mosquitto-dev/+archive/ubuntu/mosquitto-ppa/" >/dev/null 2>&1
else
    sudo wget "http://repo.mosquitto.org/${linuxDist}/${mosquitto_key_filename}" >/dev/null 2>&1
fi

sudo apt-key add "${mosquitto_key_filename}"
sudo rm "${mosquitto_key_filename}"

last_dir=$(pwd);
cd /etc/apt/sources.list.d/

# get mosquitto list
h2 "linux codename found: $h2${codename}$no"
mosquitto_url="http://repo.mosquitto.org/${linuxDist}/mosquitto-${codename}.list"

# remove last release list
if [[ -f "${apt_repo_dir}/${mosquitto_list_filename}" ]]; then
    h2 "Removing old list file : $h2$mosquitto_list_filename$no"
    sudo rm "${apt_repo_dir}/${mosquitto_list_filename}"
fi

# get the new list
h2 "Downloading mosquitto from $h2$mosquitto_url$no"
sudo wget "${mosquitto_url}" >/dev/null 2>&1

h2 "Updating APT to pick up mosquitto versions"
sudo apt-get update -qq -y

h2 "Installing mosquitto"
sudo apt-get install mosquitto -y -qq
sudo apt-get install mosquitto-clients -y -qq

# remove gpg keys, we dont need it
cd "${last_dir}"

# Copy mosquitto.config
h2 "Copying config and password files"
cp ./mosquitto.homecheck.conf /etc/mosquitto/conf.d
cp ./mosquitto_passwords.txt /etc/mosquitto

# Restarting service
sudo systemctl restart mosquitto