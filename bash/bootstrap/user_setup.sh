#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

create_user "$general_username" "$general_user_password" "$general_user_home"
create_user "$deployer_username" "$deployer_password" "$deployer_home"
h2 "If you need deployer in jenkins jobs you can remove password by typing \n sudo passwd -d ${deployer_username}"
# **********************************************************************************************************************************************************************
# Set deployer rights
cd "$deployer_home" # goto deployer home directory
h1 "Setting $h2$deployer_username$no rights"

# create sftp_users group and add $deployer_username to the group
if grep -q sftp_user /etc/group
then
  h2 "Group $h2$sftp_group_name$no exists"
else
  h2 "Group $h2$sftp_group_name$no does not exists, creating"
  sudo groupadd "$sftp_group_name"
fi

h2 "Setting user rights"
sudo usermod -aG "$sftp_group_name" "$deployer_username"
sudo usermod -g "$deployer_username" "$deployer_username"

# **********************************************************************************************************************************************************************
h1 "Writing ssh keys to users..."
cd "$deployer_home"
# create prod directory, and set owner
create_dir "$deployer_home/$ssh_dir_name" "$deployer_username"
cd "$deployer_home/$ssh_dir_name"

cat <<EOF >>authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAonP/fk9CMaZOsDHiHV4jIogIGTnW4NmN4Uo+Ec7YaFBnZqy5uGwIDWtEC3oCCgOq0DZGPQ54Ns01S0ixS0YEHkH6PUC0qmWBUUKUzjjXx+uAGKGYGi7TUxNUaAjeoTsJfq3E+2UIUb0dcPvWepgSfmAvm4zC63gy80m/tvQd3D/+76/PoqMsL+Fr+GbqVWsCVWijh+ULJoO1roSx1F0kdfawpWPbDacXWB0/lXO2mbldkXZQOGdMbEJHSNSL1af22Q6rEzVMLnR0+3mpXIbifpY2sYNADV7VZ89HauTZU5HNdprnqPHZf5YHVQ/OJVIEEHQRPZsirmfLZWL1LFptVQ== digitalocean | SEN.1
ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAktkQ0RwTtqn/zpLG7k0cpCW3eqL//uXQOKIq2YvAZ1d+sx5ZR9X7GlXzWfHmcCuxmTGJuuzV28Bk5h5c1HWaj2p7lNO0SxOQ/cDKFEphnTVzosC19GeZ3hMye/E2oYCsQGT+G1NSFVXEsx/sgosjoWMo6WxwiOtF03fNRVXEc0FrOy6mZDNOL0Vbdtu7jqa/DazRJxQspAXO9/dIyZm6lEHzmb3OfVrylvqu1nVTVPbHZmGkmqlzXccA0hXR6HNBDawFW869wW7iO4FUosBJE8WA9uhnkqa84jwO8TijANFAg/XuvHOurgAlhewsJiew8riTUjXdi+f9hiPBB4v+jw== digitalocean.deploy | deploy key for $deployer_username and bitbucket
EOF

# **********************************************************************************************************************************************************************
# Setup General User (tkovari)
h1 "Setting $h2$general_username$no rights"
# create .ssh directory for $general_username, and set owner
create_dir "$general_user_home/$ssh_dir_name" "$general_username"
# copy ssh keys from $deployer_username
sudo cp "$deployer_home/$ssh_dir_name/authorized_keys" "$general_user_home/$ssh_dir_name"
