#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

env=$1
isRaspi=$2
linuxDist=$3
codename=$4

couchdb_username="admin"
couchdb_password="sENTinel.647.Geek"

# ----------------------------------------------------------------------------------------------------------------------
h1 "Installing CouchDB"

h2 "Updating & upgrading APT"
apt update && apt upgrade -y

h2 "Installing tools (curl, apt-transport-https, gnupg)"
apt install -y curl apt-transport-https gnupg

h2 "Getting GPG key into file"
curl https://couchdb.apache.org/repo/keys.asc | gpg --dearmor | tee /usr/share/keyrings/couchdb-archive-keyring.gpg >/dev/null 2>&1

h2 "Updating APT source list"
source /etc/os-release
echo "deb [signed-by=/usr/share/keyrings/couchdb-archive-keyring.gpg] https://apache.jfrog.io/artifactory/couchdb-deb/ ${VERSION_CODENAME} main" | tee /etc/apt/sources.list.d/couchdb.list >/dev/null

h2 "Installing CouchDB"
apt update && apt install -qy -qq couchdb

# ----------------------------------------------------------------------------------------------------------------------
h1 "Installing NGINX"

h2 "Install nginx and apache-utils"
apt-get install -y nginx apache2-utils

h2 "Adding user 'admin' to system, please enter password"
htpasswd -c /etc/nginx/.htpasswd admin

h2 "Copying nginx config to /etc/nginx/sites-enabled"
sudo cp ./nginx.couchdb-admin.conf /etc/nginx/sites-enabled

h2 "Reloading nginx service to apply changes"
service nginx reload