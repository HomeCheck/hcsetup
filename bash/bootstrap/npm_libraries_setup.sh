#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

sudo export PATH="$PATH:/root/.nvm/versions/node/v19.0.1/bin"
echo "PATH::: $PATH";

# Install typescript and ts-node
h1 "Installing typescript"
npm install -g --silent typescript ts-node

# Install PM2
h1 "Installing PM2 monitor"
npm install -g  pm2 --silent
