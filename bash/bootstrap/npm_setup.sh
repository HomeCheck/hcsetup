#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

linuxDist=$3

h1 "Installing nvm..."

curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[[ -s "$NVM_DIR/nvm.sh" ]] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[[ -s "$NVM_DIR/bash_completion" ]] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

    source ~/.bashrc

h1 "Installing node and npm"
nvm install node

#update nodejs to the latest
npm cache clean -f --silent
npm install -g n --silent
curl -sL https://deb.nodesource.com/setup_current.x | sudo bash -
sudo apt-get install nodejs -qq --quiet
