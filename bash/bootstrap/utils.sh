#!/bin/sh

red="\e[0;31m"
green="\e[0;32m"
blue="\e[0;34m"
lgray="\e[0;37m"
cyan="\e[0;36m"
purple="\e[0;35m"
lblue="\e[94m"
bold="\e1[m"
underline="\e4[m"
blink="\e5[m"
invert="\e7[m"

level1="$lblue"
level2="$purple"
level3="$green"
h1="$level1"
h2="$level2"
h3="$level3"
b="$purple"
no="$lgray"

h1() {
    echo -e "\n$level1* $1$no"
}

h2() {
    echo -e "$level2  * $1$no"
}

h3() {
    echo -e "$level3    * $1$no"
}

error() {
    echo -e "$red ERROR: $1$no"
}

# creates a new user and sets home directory and password
# @param username
# @param password
# @param homes
create_user() {
  local username="$1" 
  local password="$2"
  local home=$3
  h1 "Adding new user: $username"
  if getent passwd "$username" > /dev/null 2>&1; then
    h2 "User $username already exists, no action done"
  else
    h2 "Setting up user $username"
    sudo adduser "$username" --home "$home" --quiet --disabled-password --gecos "User"
    echo "$username:$password" | sudo chpasswd
  fi

  # make it a sudoer
  h2 "Setting up user rights"
  sudo usermod -aG sudo "$username"
  sudo usermod -g "$username" "$username"
}

# creates a directory and sets up owner and group
create_dir() {
  local dir_path="$1"
  local dir_owner="$2"
  # create log directory
  h1 "Creating directory $dir_path"
  if [[ -d "$dir_path" ]]; then
    h2 "$h2$dir_path$no already exist, no action done"
  else 
    h2 "$h2$dir_path$no does not exist, creating dir"
    sudo mkdir -p -- "$dir_path"
  fi

  sudo chown "$dir_owner" "$dir_path"
  sudo chgrp "$dir_owner" "$dir_path"
}

# returns ip address
# warning: network adapter may be other than eth0, please check
get_ip_addr() {
  device=${1:-"eth0"}
  local result="$(ifconfig $device | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')"
  echo "$result"
}

wait_for_keypress() {
local title="$1"
read -p "$title" -n1 -s
}

# change a value in a file // NOT FINISHED
find_and_change() {
    filename=$1
    regex=$2
    replace=$3
    foundStr=${4:-"config found"}
    notFoundStr=${5:-"config not found"}

    if grep -q "$regex" "$filename"; then
        h3 "${foundStr}"

    else
        h3 "${notFoundStr}"
    fi
}