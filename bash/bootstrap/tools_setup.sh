#!/bin/bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

linuxDist=$3

# Install git
h1 "Installing git"
apt-get install -y -qq git

# Install Midnight Commander
h1 "Installing Midnight Commander"
sudo apt-get -y -qq --quiet install mc

# Misc tools
h1 "Installing misc tools (apt-transport-https, curl, gnupg2, lsb-release etc) ..."
sudo apt-get -y -qq install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    gnupg \
    lsb-release \
    net-tools \
    software-properties-common

h1 "Installing Docker"
# adding docker official gpg key
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/"${linuxDist}"/gpg | sudo gpg --dearmor --yes -o /etc/apt/keyrings/docker.gpg

# Setting up docker repository
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/"${linuxDist}" \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# updating apt and installing docker
sudo apt-get update -y -qq --quiet 
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y -qq --quiet
