


#mongo_setup


get_mongo_config_path() {
  local mongodb_config_path="/etc/mongod.conf"
  isOldMongo=false  
  
  if [[ ! -f "$mongodb_config_path" ]]; then
    mongodb_config_path="/etc/mongodb.conf"
    isOldMongo=true
  fi

  echo $mongodb_config_path
}

# todo: CHECK WHETHER ITS WORKING IN VAGRANT AND prod as well!!!
prodVolumeName="volume_fra1_01"
prodVolumeDir="/mnt/${prodVolumeName}"
prodVolume="$prodVolumeDir/mongodb"
otherVolume="/var/lib/mongodb"

# deploy_setup
deployer_dir="/www"
deployer_username="deployer"
deployer_password="Sentinel.123"
deployer_home="/www"

#server deploy
upload_dir="$deployer_dir/upload"
prod_dir="$deployer_dir/prod"
deploy_log_dir="$deployer_dir/log"

server_deploy_dir="$deployer_dir/server"
server_upload_dir="$upload_dir/server"
server_prod_dir="$server_deploy_dir/prod"
server_log_dir="$server_deploy_dir/log"
server_script_dir="$server_deploy_dir/scripts"

# user_setup
general_username="tkovari"
general_user_password="Sentinel.123"
general_user_home="/home/$general_username"


sftp_group_name="sftp_users"
ssh_dir_name=".ssh"

#tools_setup
codename=$(lsb_release -a 2>&1 | grep Codename: | sed -e 's/Codename:\s*//g')
linux_dist=$(lsb_release -a 2>&1 | grep Distributor | sed -e 's/Distributor ID:\s*//g')
linux_dist="${linux_dist,,}"
apt_repo_dir="/etc/apt/sources.list.d"


