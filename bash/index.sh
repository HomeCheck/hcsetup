#!/usr/bin/env bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

whiptail --msgbox "Welcome to HomeCheck Setup for Debian/Raspbian. This will lead you through the installation of HomeCheck Modules."
wait_for_keypress "Press any key..."


Modules=$(whiptail --title "Test Checklist Dialog" --checklist \
"What is the Linux distro of your choice?" 15 60 4 \
"debian" "Venerable Debian" ON \
"ubuntu" "Popular Ubuntu" ON \
"centos" "Stable CentOS" ON \
"mint" "Rising Star Mint" OFF 3>&1 1>&2 2>&3)

exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "The chosen distro is:" $DISTROS
else
    echo "You chose Cancel."
fi