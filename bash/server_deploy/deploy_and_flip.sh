#!/usr/bin/env bash
#version 0.2.0b

sourceDir="$1"        # /www/upload/server
deployFolder="$2"     # /www/server/
liveDir="$3"          # /www/server/prod

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

# --------------------------------------------------------------------------------------------
h1 "Deploy Params"
h2 "Source directory: $sourceDir"  #/www/upload/server
h2 "Deploy directory: $deployFolder" #/www/server
h2 "Prod directory: $liveDir" #/www/server/prod

# --------------------------------------------------------------------------------------------
#check deploy directory exists
if [[ ! -d "$sourceDir" ]]; then
    error "deploy source directory does not exist ($sourceDir)"
    exit 1
fi

h1 "Deploying directory $1 to base directory $2"

# get current date
date=$(date +%F-%H-%M-%S_%N)
deployDestFolder="${deployFolder}/$date"    #/www/server/2022_10_28_13234412


# --------------------------------------------------------------------------------------------
h1 "Creating deploy directory: $deployDestFolder"
if [[ -d "$deployDestFolder" ]]; then
    error "destionation folder already exist, exiting."
    exit 1
fi

mkdir "$deployDestFolder"

if [[ ! -d "$deployDestFolder" ]]; then
    echo "Cannot create folder $deployDestFolder, exiting"
    exit 1
fi

# --------------------------------------------------------------------------------------------
h1 "copying source files from $sourceDir/. to $deployDestFolder"
cp "${sourceDir}"/.  "$deployDestFolder" "-R"
chmod -R 771 "$deployDestFolder"
chgrp -R "$deployer_username" "$deployDestFolder"

# check last operation result ($?)
if [[ $? != 0 ]]; then
    echo "Copy was  not successful, exiting"
    exit 1
fi

# --------------------------------------------------------------------------------------------
h1 "Removing files from source directory";
# only delete if sourceDir exists and length > 8 (making sure it's not root or some base dir)
if [[ ${#sourceDir} -gt 8 ]]; then
    echo "removing commented out"
    rm -rf "${sourceDir}"/*
else
    echo "suspicious rm command, exiting"
    exit 1
fi

# --------------------------------------------------------------------------------------------
h1 "Flip symlink to the latest deploy"
ln -sfn "$deployDestFolder" "$liveDir"