#!/usr/bin/env bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

siteName="$1"
artifactPath="$upload_dir/build-artifact.tar.gz"
extractDir="$upload_dir/$siteName"
deployDir="/var/www/$siteName"
liveDirPath="/var/www/$siteName/www"
tempDir="/var/tmp/$siteName"
dataSourceDir="$liveDirPath/assets/data"

echo "Current dir: $(pwd)"
echo "Site to deploy: ${siteName}"

# --- [ exit if server upload dir does not exist (why?) ] ----------------------
if [[ ! -d "$server_upload_dir" ]]; then
    error "Server upload dir has not been set or does not exist, exiting"
    exit 1
fi

# ------------------------------------------------------------------------------
# if artifact file exists
if [[ -f "$artifactPath" ]]; then
    # remove previous upload
    rm -rf "$extractDir"/*
    # unzip files
    tar -xf "$artifactPath" -C "$extractDir"
else
    error "Artifact does not exist, deploy failed"
    exit 1
fi

# --- [ Copy data to temp ] ----------------------------------------------------
if [[ ! -d "$tempDir" ]]; then
    create_dir "$tempDir" "$deployer_username"
    chmod 775 "$tempDir"     # group also can read/write/execute
fi

# --- [ Exit if temp dir was not created ] -------------------------------------
if [[ ! -d "$tempDir" ]]; then
    echo "Temporary dir has not been set or does not exist, exiting"
    exit 1
fi


# ------------------------------------------------------------------------------
h1 "Removing all files from temp directory"

if [[ -d "$dataSourceDir" ]]; then
    rm -rf "$tempDir"/*

    if [[ $? != 0 ]]; then
        error "Removing files from temp dir failed"
        exit 1
    fi
else
    h2 "Temp dir does not exist, no files were removed"
fi

# ------------------------------------------------------------------------------
h1 "Copy data JSON files to tmp"
if [[ -d "$dataSourceDir" ]]; then
    mv "$dataSourceDir"/* "$tempDir"

    if [[ $? != 0 ]]; then
        error "Copying files to temp dir failed"
        exit 1
    fi
else
    h2 "data source folder does not exist, no files will be copied"
fi

# ------------------------------------------------------------------------------
# execute deploy script
bash "./server_deploy/deploy_and_flip.sh" "$extractDir" "$deployDir" "$liveDirPath"

# ------------------------------------------------------------------------------
h1 "Copy data JSON files from tmp to prod dir"
if [[ -d "$dataSourceDir" ]]; then
    mv "$tempDir"/* "$dataSourceDir"

    if [[ $? != 0 ]]; then
        error "Copying files from temp dir failed"
        exit 1
    fi
else
    h2 "data source folder does not exist, no files will be copied"
fi
# ------------------------------------------------------------------------------