#!/usr/bin/env bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

liveDirName="prod"
app_name="helyzet.eu-provider"
artifactFilename="build-artifact.tar.gz"

# deploy_dir: directory where app will be deployed
deploy_dir="/www/prod/$app_name"

# upload_dir: where artifact tar is found
upload_dir="/www/upload/$app_name"

# liveDirPath: prduction folder
liveDirPath="$deploy_dir/$liveDirName"

# artifactFilePath: tar file name
artifactFilePath="/www/upload/$artifactFilename"

# tempDir: temporary directory
tempDir="/var/tmp/helyzet.eu-provider"

# dataSourceDir: previous data folder
dataSourceDir="$liveDirPath"/output

# --- [ Check & create server upload dir ] ------------------------------------------------------
if [[ ! -d "$upload_dir" ]]; then
    create_dir "$upload_dir" "$deployer_username"
    chmod 775 "$upload_dir"     # group also can read/write/execute
fi

if [[ ! -d "$upload_dir" ]]; then
    echo "Provider upload dir has not been set or does not exist, exiting"
    exit 1
fi

# --- [ Check & create server deploy dir ] ------------------------------------------------------
if [[ ! -d "$deploy_dir" ]]; then
    create_dir "$deploy_dir" "$deployer_username"
    chmod 775 "$deploy_dir"     # group also can read/write/execute
fi

# --- [ Exit if deploy dir was not created ] ----------------------------------------------------
if [[ ! -d "$deploy_dir" ]]; then
    echo "Provider deploy dir has not been set or does not exist, exiting"
    exit 1
fi

# --- [ Copy data to temp ] ------------------------------------------------------
if [[ ! -d "$tempDir" ]]; then
    create_dir "$tempDir" "$deployer_username"
    chmod 775 "$tempDir"     # group also can read/write/execute
fi

# --- [ Exit if temp dir was not created ] ----------------------------------------------------
if [[ ! -d "$tempDir" ]]; then
    echo "Temporary dir has not been set or does not exist, exiting"
    exit 1
fi

h1 "Removing all files from temp directory"
rm -rf "$tempDir"/*

if [[ $? != 0 ]]; then
    error "Removing files from temp dir failed"
    exit 1
fi


h1 "Copy data JSON files to tmp"
mv "$dataSourceDir"/* "$tempDir"

if [[ $? != 0 ]]; then
    error "Copying files to temp dir failed"
    exit 1
fi

# --- [ Remove previous upload and unzip ] ------------------------------------------------------
if [[ -f "$artifactFilePath" ]]; then
    h1 "Removing previous files from $upload_dir"
    rm -rf "$upload_dir"/*

    h1 "Untar artifact file to $upload_dir"
    # unzip files
    tar -xf "$artifactFilePath" -C "$upload_dir"
else
    error "Artifact file '$artifactFilePath' not found, exiting"
    exit 1
fi

# --- [ Execute deploy script ] -----------------------------------------------------------------
bash "./server_deploy/deploy_and_flip.sh" "$upload_dir" "$deploy_dir" "$liveDirPath"

# --- [ copy data files from temp ] -------------------------------------------------------------
h1 "Copy data JSON files from temp to prod"
mv "$tempDir"/* "$dataSourceDir"

if [[ $? != 0 ]]; then
    error "Copying files from temp dir to prod failed"
    exit 1
fi

# --- [ Restarting pm2  ] -----------------------------------------------------------------------
h1 "Restart pm2... with user '${deployer_username}'"
cd "${liveDirPath}"
sudo -u ${deployer_username} pm2 restart "${liveDirPath}"/config/ecosystem.config.js

if [[ $? != 0 ]]; then
    error "starting pm2 failed"
    exit 1
fi

# --- [ PM2 save ] ------------------------------------------------------------------------------
h1 "pm2 save"
sudo -u ${deployer_username} pm2 save

if [[ $? != 0 ]]; then
    error "pm2 save failed"
    exit 1
fi
