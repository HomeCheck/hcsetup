#!/usr/bin/env bash

source ./bootstrap/utils.sh
source ./bootstrap/variables.sh

liveDirName="prod"
liveDirPath="$server_deploy_dir/$liveDirName"
artifactFilename="$upload_dir"/build-artifact.tar.gz

echo "Deploy Server will deploy HomeCheck Server from the following artifact: $artifactFilename"

# --------------------------------------------------------------------------------------------
# check whether server upload dir exists
if [[ ! -d "$server_upload_dir" ]]; then
    error "Server upload dir has not been set or does not exist, exiting"
    exit 1
fi

# --------------------------------------------------------------------------------------------
# remove previous upload if bitbucket

h1 "Preparing environment"
if [[ -f "$artifactFilename" ]]; then
    h2 "Removing previous upload"
    rm -rf "$server_upload_dir"/*

    if [[ $? != 0 ]]; then
        error "Removing directory was unsuccessful, see details above, exiting."
        exit 1
    fi

    # unzip files
    h2 "Untar file '$artifactFilename' to folder '$server_upload_dir'"
    tar -xf "$artifactFilename" -C "$server_upload_dir"
fi

if [[ $? != 0 ]]; then
    error "Untar command was unsuccessful, see details above, exiting."
    exit 1
fi


# --------------------------------------------------------------------------------------------
# execute deploy script
h1 "Deploying and flipping symlink using upload folder '$server_upload_dir' to '$server_deploy_dir' inside folder '$liveDirPath'";
bash "./server_deploy/deploy_and_flip.sh" "$server_upload_dir" "$server_deploy_dir" "$liveDirPath"

if [[ $? != 0 ]]; then
    error "Deployment was unsuccessful, see details above, exiting."
    exit 1
fi


# --------------------------------------------------------------------------------------------
h1 "Post actions"
h2 "Creating mode file"
echo "prod" > "${liveDirPath}"/config/mode

# check last operation result ($?)
h2 "Checking whether creating mode file was successful"
if [[ $? != 0 ]]; then
    error "Creating mod file was not successful, exiting"
    exit 1
fi

# --------------------------------------------------------------------------------------------
h1 "PM2"
h2 "Restarting pm2... as user '${deployer_username}'"
cd "${liveDirPath}"
sudo -u ${deployer_username} pm2 restart "${liveDirPath}"/config/ecosystem.config.js

if [[ $? != 0 ]]; then
    error "Starting pm2 failed, exiting"
    exit 1
fi

# --------------------------------------------------------------------------------------------
h2 "Saving PM2 list for later use"
sudo -u ${deployer_username} pm2 save
if [[ $? != 0 ]]; then

    exit 1
fi